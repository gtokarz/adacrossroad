with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Numerics.Elementary_Functions;
use  Ada.Numerics.Elementary_Functions;
package body Utils is
   procedure Put(O: TPoint2D) is
   begin
      Ada.Text_IO.Put(Float'Image(O.x)&","&Float'Image(O.y));
   end Put;
   procedure Put(O: TVec2D) is
   begin
      Ada.Text_IO.Put(Float'Image(O.dx)&","&Float'Image(O.dy));
   end Put;
    function distance(this: TPoint2D'Class; other: TPoint2D'Class) return Float is
   begin
      return Sqrt((this.x-other.x)**2.0+(this.y-other.y)**2.0);
   end distance;
   function distanceVec(this: TPoint2D'Class; other: TPoint2D'Class) return TVec2D is
      result:TVec2D;
   begin
      result:=((other.x-this.x),(other.y-this.y));
      return result;
   end distanceVec;
   procedure normalize(this: in out TVec2D) is
      length:Float:=1.0;
   begin
      length:=Sqrt(this.dx*this.dx+this.dy*this.dy);
      this.dx:=this.dx/length;
      this.dy:=this.dy/length;
   end normalize;
   procedure moveAlongVec(this: out TPoint2D'Class;vec: in TVec2D'Class) is
   begin
      this.x:=this.x+vec.dx;
      this.y:=this.y+vec.dy;
   end moveAlongVec;
   function length(this: in TVec2D'Class) return Float is
   begin
      return Sqrt(this.dx*this.dx+this.dy*this.dy);
   end length;
   procedure multiply(this: out TVec2D; factor:Float) is
   begin
      this.dx:=this.dx*factor;
      this.dy:=this.dy*factor;
   end multiply;
   procedure simpleCallBack is
   begin
      Put_Line("TOD callback");
   end simpleCallBack;
end Utils;
