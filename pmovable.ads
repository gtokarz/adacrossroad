--Bazowa klasa obiektu zdolnego poruszać się w przestrzeni 2D
with PEntity;
package PMovable is
type TMovable is new PEntity.TEntity with record
      velocity:Float:=0.0;
      acceleration:Float:=0.0;
      deceleration:Float:=0.0;
   end record;
end PMovable;
