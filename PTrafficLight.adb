with Ada.Text_IO;
use Ada.Text_IO;
with Utils;
use Utils;
with Ada.Numerics.Elementary_Functions;
use  Ada.Numerics.Elementary_Functions;
with Ada.Calendar;
use Ada.Calendar;
with Simulation;
with PTimeOfDay.TimeOfDayClockAPI; use PTimeOfDay.TimeOfDayClockAPI;
with Ada.Exceptions; use Ada.Exceptions;
package body PTrafficLight is
   protected body TTrafficLightProtection is
      function getLight(index: in TLightCount_Range) return TTrafficLight_Enum is
      begin
         return ref(index).actualLight;
      end getLight;
      procedure setLight(index: in TLightCount_Range; newstate: TTrafficLight_Enum) is
      begin
         ref(index).actualLight:=newstate;
      end setLight;
      function getPos(index: in TLightCount_Range) return TPoint2D is
      begin
         return ref(index).pos;
      end getPos;
      procedure setPos(index: in TLightCount_Range; newpos: TPoint2D) is
      begin
         ref(index).pos:=newpos;
      end setPos;
      procedure Init(index: in TLightCount_Range;xPoint,yPoint: Float) is
      begin
         ref(index).Init(xPoint,yPoint);
      end Init;
      function Length return Integer is
      begin
         return ref'Length;
      end Length;
   end TTrafficLightProtection;
   --ada2java nie wspiera protected - proste API
   package body TTrafficLightProtectionAPI is
      function getLight(index: in TLightCount_Range) return TTrafficLight_Enum is
      begin
         return lightTab.getLight(index);
      end getLight;
      procedure setLight(index: in TLightCount_Range; newstate: TTrafficLight_Enum) is
      begin
         lightTab.setLight(index,newstate);
      end setLight;
      function getPos(index: in TLightCount_Range) return TPoint2D is
      begin
         return lightTab.getPos(index);
      end getPos;
      procedure setPos(index: in TLightCount_Range; newpos: TPoint2D) is
      begin
         lightTab.setPos(index,newpos);
      end setPos;
      procedure Init(index: in TLightCount_Range;xPoint,yPoint: Float) is
      begin
         lightTab.Init(index,xPoint,yPoint);
      end Init;
      function Length return Integer is
      begin
         return lightTab.Length;
      end Length;
   end TTrafficLightProtectionAPI;

   procedure setLight(tlO: out TTrafficLight; lightToSet: in TTrafficLight_Enum) is
   begin
      tlO.actualLight := lightToSet;
   end setLight;

   procedure Init(this: out TTrafficLight; xPoint: in Float; yPoint: in Float) is
   begin
      this.pos.x := xPoint;
      this.pos.y := yPoint;
      this.actualLight := Green;

   end Init;

   procedure setLight is
   begin
      lightTab.Init(1,xPoint => 40.0, yPoint => 31.5);
      lightTab.Init(2,xPoint => 47.75, yPoint => 40.0);
      lightTab.Init(3,xPoint => 40.0, yPoint => 48.5);
      lightTab.Init(4,xPoint => 32.25, yPoint => 40.25);
      lightTab.Init(5,xPoint => 36.00, yPoint => 31.5);
      lightTab.Init(6,xPoint => 47.75, yPoint => 35.25);
      lightTab.Init(7,xPoint => 44.00, yPoint => 48.5);
      lightTab.Init(8,xPoint => 32.25, yPoint => 44.75);
      lightTab.Init(9,xPoint => 44.00, yPoint => 31.5);
      lightTab.Init(10,xPoint => 47.75, yPoint => 44.75);
      lightTab.Init(11,xPoint => 36.00, yPoint => 48.5);
      lightTab.Init(12,xPoint => 32.25, yPoint => 35.25);
   end setLight;

   task body lightstateChange_Task is
      Next_Time : Time;
      Final_Time:Time;
      Delta_Time: Duration;
      resumed:Boolean:=false;
      dayNightI:Integer:=TTrafficLight_DayNight'Pos(dayNight)+1;
   begin
      setLight;
      main_loop:
      loop
         select
            accept Resume do
               resumed:=true;
            end Resume;
         or
            accept Start;
         or
            accept Stop;
            exit main_loop;
         end select;
         run_loop:
         loop
            --fix synchro czasu po Resume
            if not resumed then
               Delta_Time:=Duration(Simulation.duration2sim(statesTab(dayNightI)(currentState).waitTime));
               Final_Time:=Clock+Delta_Time;
            else
               Delta_Time:=Final_Time-Clock;
               resumed:=false;
            end if;
            dayNightI:=TTrafficLight_DayNight'Pos(dayNight)+1;
            --przelacz stan zgodnie z tabela
            for I in statesTab(dayNightI)(currentState).lightStates'Range loop
               lightTab.setLight(I,statesTab(dayNightI)(currentState).lightStates(I));
            end loop;
            --chunked delay
            loop
               Next_Time := MinimalTime(Clock + Duration(1.0/Simulation.speed),Clock + Delta_Time); --kawalki max dlugie jak 1/speed
               select
                  accept Stop;
                  exit main_loop;
               or
                  accept Pause;
                  exit run_loop;
               else
                  Null;
               end select;
               delay until Next_Time;
               Delta_Time:=Final_Time-Clock;
               exit when Clock>=Final_Time;
            end loop;
            currentState:=currentState+1;
            if currentState>statesTab(dayNightI)'Length then
               currentState:=1;
            end if;
         end loop run_loop;
      end loop main_loop;
   exception
      when Error:others =>
         Put_Line("It shouldn't happen: "&Exception_Information(Error)&":"&Exception_Message(Error));
   end lightstateChange_Task;

end PTrafficLight;
