rmdir /s /q java_code\src
rmdir /s /q glue_code
for %%i in (gnat.exe) do set GNAT_PATH=%%~dp$PATH:i

set ADA_PROJECT_PATH=%GNAT_PATH%..\lib\gnat
set GPR_PROJECT_PATH=%ADA_PROJECT_PATH%
ada2java.exe *.ads -L hello_proj -c java_code/src -o glue_code --library-kind=encapsulated > generate.log
patch < patches/libraryLoaderPatch.patch -p 1
patch < patches/asyncTaskManager.patch -p 1

dir /s /B java_code\*.java > sources.txt
javac @sources.txt -cp lib\ajis.jar
patch -d %GPR_PROJECT_PATH% <patches/ajis.patch
patch -d %GPR_PROJECT_PATH% <patches/jni.patch
gprbuild -j10 -p -P glue_code\hello_proj.gpr -gnat2012
patch -d %GPR_PROJECT_PATH% -R<patches/ajis.patch
patch -d %GPR_PROJECT_PATH% -R<patches/jni.patch
copy /Y "glue_code\lib\*" "lib\"
