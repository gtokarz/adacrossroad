With Ada.Text_IO; Use Ada.Text_IO;
With Simulation;
With Utils; Use Utils;
With Ada.Numerics.Elementary_Functions; Use  Ada.Numerics.Elementary_Functions;
With PRoad;
With PTrafficLight; Use PTrafficLight;
With Ada.Numerics.Float_Random;
With Ada.Exceptions; Use Ada.Exceptions;
package body PCar is
   procedure setLeader(O: out TCar;leader: in TCar) is
   begin
      O.leadingCar:=leader'Unrestricted_Access;
   end setLeader;
   procedure setLeader(O: out TCar;leader: in TCar_Ptr) is
   begin
      O.leadingCar:=leader;
   end setLeader;
   --ustawia startowe parametry samochodu
   procedure Init(this: out TCar) is
   begin
      this.velocity:=0.0;
      this.velocityCap:=defaultVelocityCap*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*velocityCapSTDdev*2.0);
      this.acceleration:=defaultAcceleration*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*accelerationSTDdev*2.0);
      this.deceleration:=defaultDeceleration*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*decelerationSTDdev*2.0);
      this.T:=defaultHeadway;
      this.minDistance:=defaultMinDistance*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*minDistanceSTDdev*2.0);
      this.length:=defaultLength;--*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*lendthSTDdev*2.0);
      this.icon:=Simulation.Rand_Int.Random(Simulation.intGenerator);
   end Init;
   procedure move(this: out TCar) is
      distance:Float:=Float'Last; --matematyka taka zła: +Inf
      distanceTMP:Float:=Float'Last; --matematyka taka zła: +Inf
      tmpDistance:TVec2D:=(0.0,0.0);
      freeWayFactor:Float:=0.0;
      interactionFactor:Float:=0.0;
      velocityDelta:Float:=this.velocity;
   begin
      if this.pos.y>80.0 or this.pos.y<0.0 or this.pos.x>80.0 or this.pos.x<0.0 then
         --despawn auta, oznacz jako do usuniecia i pozwol watkowi posprzatac
         if this.followingCar /=null then
            this.followingCar.leadingCar:=null;
            this.toDelete:=true;
         end if;
         Null;
      end if;
      --rozwaz hamowanie przy czerwonym i zoltym, nastepnie sprawdz czy jest przed nami
      if (lightTab.getLight(this.laneIndex) = Red or lightTab.getLight(this.laneIndex) = Yellow) and this.trafficLightIsAtFront(this.laneIndex) then
         distanceTMP:=this.pos.distanceVec(lightTab.getPos(this.laneIndex)).length; --odleglosc do swiatla
      end if;
      if this.leadingCar /= null then
         tmpDistance:=this.distanceVec(this.leadingCar.all);
         distance:=tmpDistance.length;
         velocityDelta:=this.velocity-this.leadingCar.velocity;
      end if;

      --jak dobrze poszlo to mamy odleglosci do swiatel i samochodu przed
      if distanceTMP>0.0 and distanceTMP<distance then --swiatlo jest blizej, niz samochod zdecyduj czy hamowac
         interactionFactor:=-this.acceleration*(((this.minDistance+this.velocity*this.T)/distanceTMP + (this.velocity*this.velocity)/(2.0*Sqrt(this.acceleration*this.deceleration)*distanceTMP))**2);--[m/s^2]
         if (lightTab.getLight(this.laneIndex) = Red or lightTab.getLight(this.laneIndex) = Yellow) then --mamy zolte albo czerwone - hamuj
            distance:=distanceTMP;
         end if;
      else
         --przelicz od nowa wspolczynnik
         interactionFactor:=-this.acceleration*(((this.minDistance+this.velocity*this.T)/distance + (this.velocity*velocityDelta)/(2.0*Sqrt(this.acceleration*this.deceleration)*distance))**2);--[m/s^2]
      end if;

      if distance'Valid and not (distance=0.0) then
         --[m/s^2]
         freeWayFactor:=this.acceleration*(1.0-(this.velocity/this.velocityCap)**Integer(deltaExponent));
         --[m/s^2]
         velocityDelta:=freeWayFactor+interactionFactor;
         velocityDelta:=Simulation.scaleTimeDomain(freeWayFactor+interactionFactor);
         --update velocity
         this.velocity:=this.velocity+velocityDelta;
         if this.velocity<0.0 then
            this.velocity:=0.0;
         end if;
         --trajectory
         tmpDistance:=PRoad.spawnPoints(this.laneIndex).trajectory;
         tmpDistance.normalize;
         --scale time domain for velocity & prepare movement vector
         tmpDistance.multiply(Simulation.scaleTimeDomain(this.velocity));
         --move car along vector
         this.pos.moveAlongVec(tmpDistance);
      end if;

   end move;
   --czy swiatlo jest widziane +/-45 od wektora zwrotu
   function trafficLightIsAtFront(this: in TCar; lightIndex: TLightCount_Range) return Boolean is
      lightTrajectory:TVec2D:=(0.0,0.0);
      pitch:Float:=0.0;
      lightTrajectoryAngle:Float:=0.0;
      bounds: array(1..2) of Float:=(0.0,0.0);
   begin
      lightTrajectory:=this.pos.distanceVec(lightTab.getPos(lightIndex));
      begin
         lightTrajectoryAngle:=Arctan(lightTrajectory.dx,lightTrajectory.dy);--zakres -PI;PI
         pitch:=Arctan(this.pitch.dx,this.pitch.dy);--zalres -PI;PI
      exception
         when ADA.NUMERICS.ARGUMENT_ERROR => --jak odleglosc = 0 arctan sie wylozy :)
            return false;
      end;
      bounds(1):=(pitch+0.25*Ada.Numerics.PI);
      bounds(2):=(pitch-0.25*Ada.Numerics.PI);
      --skalowanie zakresu "obserwacji"
      if bounds(1)>=Ada.Numerics.PI then
         bounds(1):=bounds(1)-2.0*Ada.Numerics.PI;
         bounds(2):=bounds(2)-2.0*Ada.Numerics.PI;
      elsif bounds(2)<-Ada.Numerics.PI then
         bounds(1):=bounds(1)+2.0*Ada.Numerics.PI;
         bounds(2):=bounds(2)+2.0*Ada.Numerics.PI;
      end if;
      if lightTrajectoryAngle<bounds(1) and lightTrajectoryAngle>bounds(2) then
         return true;
      else
         return False;
      end if;

   end;
   --odleglosc do samochodu
   function distance(this: in TCar'Class; other: in TCar'Class) return Float is
   begin
      return distance(this.pos,other.pos);
   end distance;

   function distanceVec(this: in TCar'Class; other: in TCar'Class) return TVec2D is
   begin
      return distanceVec(this.pos,other.pos);
   end distanceVec;

end PCar;
