with Ada.Text_IO; use Ada.Text_IO;
with Utils; use Utils;
with PCar; use PCar;
with PRoad; use PRoad;
with Ada.Containers; use Ada.Containers;
with TaskManager;
--with Gtk.Main;
--with Gtk.Window;      use Gtk.Window;
--with Main_quit;
with PTrafficLight; use PTrafficLight;
--with max_size;

with Simulation; use Simulation;
with PTimeOfDay.TimeOfDayClockAPI; use PTimeOfDay.TimeOfDayClockAPI;
with PTimeOfDay; use PTimeOfDay;

procedure Hello is
   --todClock:TimeOfDayClock;
   --synchroTask: lightstateChange_Task;
   wrap:TimeOfDayEventCallback_Wrap:=(ptr=>simpleCallBack'Access);
begin
   Simulation.speed:=40.0;
   Simulation.stepsPerSecond:=600.0;
   PCar.defaultHeadway:=0.000006;
   Simulation.seed:=666;
     TODClock.registerCallback(1,wrap);
--     TODClock.registerCallback(1,simpleCallBack'Access);
   TaskManager.startAll;
   loop
      exit when not TaskManager.isRunning;
      delay 5.0;
      end loop;
end Hello;
