--api widoczne dla javy, aby uzywac obiektu chronionego
package PTimeOfDay.TimeOfDayClockAPI is
   function getTime return TimeOfDay;
   procedure setTime(val:TimeOfDay);
   procedure registerCallback(tod:TimeOfDay;callback:TimeOfDayEventCallback_Wrap);
   procedure resetCallbacks;
   --obiekt chroniony zegara
   protected type TimeOfDayClock is
      procedure registerCallback(tod:TimeOfDay;callback:TimeOfDayEventCallback_Wrap);
      procedure tick;
      function getTime return TimeOfDay;
      procedure setTime(val:TimeOfDay);
      procedure resetCallbacks;
   private
      eventMap:PTimeOfDayEventMap.Map;
      currentTOD:TimeOfDay:=0;
   end TimeOfDayClock;

   TODClock:TimeOfDayClock;


   type TTrafficLight_DayNight is (Day,Night);

   dayNight:TTrafficLight_DayNight:=Day;
   pragma Atomic(dayNight);
   procedure setDay;
   procedure setNight;
   setDayWrap:TimeOfDayEventCallback_Wrap:=(ptr=>setDay'Access);
   setNightWrap:TimeOfDayEventCallback_Wrap:=(ptr=>setNight'Access);
end PTimeOfDay.TimeOfDayClockAPI;
