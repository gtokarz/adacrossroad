with Ada.Unchecked_Deallocation;
with PEntity;
with PMovable;
use PEntity;
with Utils; use Utils;
with PTrafficLight; use PTRafficLight;

package PCar is
   -------IDM model constants--------------
   accelerationSTDdev:Float := 0.50;--50%
   decelerationSTDdev:Float := 0.397;--39.7%
   velocityCapSTDdev:Float := 0.08;--8%
   minDistanceSTDdev:Float :=0.1;--10%
   defaultAcceleration:Float := 1.56;--1.56m/s^2
   defaultDeceleration:Float := 4.405;--4.405m/s^2
   defaultVelocityCap:Float := 13.88;--13.88 m/s = 50km/h
   defaultHeadway:Float := 1.4;--1.5s
   deltaExponent:Float := 4.0;
   defaultMinDistance:Float := 7.8;
   defaultLength:Float:= 3.9292;--3.9292m
   lendthSTDdev:Float:=0.0615;--6.15%
   ----------------------------------------

   type TCar;
   type TCar_Ptr is access all TCar;
   type TCar is new PMovable.TMovable with
      record
         velocityCap:Float := defaultVelocityCap;
         T:Float :=defaultHeadway;
         minDistance:Float :=defaultMinDistance;
         leadingCar:TCar_Ptr:=null;
         followingCar:TCar_Ptr:=null;
         laneIndex:Integer:=0;
         pitch:aliased TVec2D:=(0.0,0.0);
         icon:Integer range 0..9;
         length:Float:=defaultLength;
         toDelete:Boolean:=False;
      end record;

   procedure setLeader(O: out TCar;leader: in TCar);

   procedure setLeader(O: out TCar;leader: in TCar_Ptr);
   --ustaw parametry samochodu na losowe itd.
   procedure Init(this: out TCar);
   --porusz samochod
   procedure move(this: out TCar);
   function distance(this: in TCar'Class; other: in TCar'Class) return Float;
   function distanceVec(this: in TCar'Class; other: in TCar'Class) return TVec2D;
   function trafficLightIsAtFront(this: in TCar; lightIndex: TLightCount_Range) return Boolean;
   procedure Free is new Ada.Unchecked_Deallocation (Object => TCar,
                                                          Name   => TCar_Ptr);
end PCar;
