with Ada.Strings.Hash;
with Simulation; use Simulation;
with Ada.Calendar; use Ada.Calendar;
with Ada.Text_IO; use Ada.Text_IO;
with PTimeOfDay.TimeOfDayClockAPI; use PTimeOfDay.TimeOfDayClockAPI;
with Ada.Exceptions; use Ada.Exceptions;
use Ada.Exceptions;
package body PTimeOfDay is
   function TimeOfDay_Hashed (tod: TimeOfDay) return Hash_Type is
   begin
      return Ada.Strings.Hash(TimeOfDay'Image(tod));
   end TimeOfDay_Hashed;

   task body TOD_Task is
      Next_Time :Time;
   begin
      main_loop:
      loop
         select
            accept Start;
         or
            accept Resume;
         or
            accept Stop;
            exit main_loop;
         end select;
         loop
            Next_Time := Clock + Duration(1.0/Simulation.speed);
            delay until Next_Time;
            TODClock.tick;
            Select
               accept Stop;
               exit main_loop;
            or
               accept Pause;
               exit;
            else
               Null;
            end select;
         end loop;
      end loop main_loop;
   exception
      when Error:others =>
         Put_Line("It shouldn't happen: "&Exception_Information(Error)&":"&Exception_Message(Error));
   end TOD_Task;
end PTimeOfDay;
