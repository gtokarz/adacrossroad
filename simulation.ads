with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Float_Random;
with Ada.Calendar; use Ada.Calendar;
with Ada.Calendar.Conversions; use Ada.Calendar.Conversions;
with PTimeOfDay; use PTimeOfDay;

package Simulation is
   subtype Rand_Range is Integer range 1..7;
   package Rand_Int is new Ada.Numerics.Discrete_Random(Rand_Range);
   package Rand_Time is new Ada.Numerics.Discrete_Random(TimeOfDay);

   intGenerator : aliased Rand_Int.Generator;
   todGenerator : aliased Rand_Time.Generator;
   floatGenerator : aliased Ada.Numerics.Float_Random.Generator;
   seed:Integer:=Integer(To_Unix_Time(Clock));
   Num : Rand_Range;
   stepsPerSecond:Float:=20.0;
   pragma Atomic(stepsPerSecond);
   speed:Float:=4.0;
   pragma Atomic(speed);
   initialized:Boolean:=False;
   pragma Atomic(initialized);
   frame:Integer:=0;
   procedure Init;
   --skaluje czasu np m/s => m/step
   function scaleTimeDomain(value: in Float) return Float;
   --skaluje czasu np m*s => m*step
   function scaleTimeDomainInv(value: in Float) return Float;
   function duration2sim(value: in Float) return Float;

end Simulation;
