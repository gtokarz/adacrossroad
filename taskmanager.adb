with Ada.Text_IO; use Ada.Text_IO;
package body TaskManager is
   procedure startAll is
   begin
      if not isRunning then
         Simulation.Init;
         lightSynchroTask:=new lightstateChange_Task;
         lightSynchroTask.Start;
         carMoverTask:=new TCarMoverTask;
         carMoverTask.Start;
         TODClock_Task:=new TOD_Task;
         TODClock_Task.Start;
         isRunning:=true;
         isPaused:=false;
      end if;
   end startAll;
   procedure killAll is
   begin
      if isRunning then
         lightSynchroTask.Stop;
         Free(lightSynchroTask);
         carMoverTask.Stop;
         Free(carMoverTask);
         TODClock_Task.Stop;
         Free(TODClock_Task);
         isRunning:=false;
         isPaused:=true;
         Simulation.initialized:=false;
      end if;
   end killAll;
   procedure pauseAll is
   begin
      if isRunning and not isPaused then
         lightSynchroTask.Pause;
         carMoverTask.Pause;
         TODClock_Task.Pause;
         isPaused:=true;
      end if;

   end pauseAll;

   procedure resumeAll is begin
      if isRunning and isPaused then
         lightSynchroTask.Resume;
         carMoverTask.Resume;
         TODClock_Task.Resume;
         isPaused:=false;
      end if;
   end resumeAll;

end TaskManager;
