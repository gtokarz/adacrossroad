with Ada.Calendar.Conversions; use Ada.Calendar.Conversions;
with PTrafficLight; use PTrafficLight;
with Ada.Text_IO; use Ada.Text_IO;
with PTimeOfDay.TimeOfDayClockAPI; use PTimeOfDay.TimeOfDayClockAPI;
with PRoad; use PRoad;
package body Simulation is

   --skaluj domenę czasu: warość na sekundę => wartość na krok
   function scaleTimeDomain(value: in Float) return Float is
   begin
      return value*speed/stepsPerSecond;
   end scaleTimeDomain;

   --skaluj domenę czasu: warość * sekundę => wartość * krok
   function scaleTimeDomainInv(value: in Float) return Float is
   begin
      return value/stepsPerSecond*speed;
   end scaleTimeDomainInv;

   function duration2sim(value: in Float) return Float is
   begin
      return value/speed;
   end duration2sim;

   procedure Init is
   begin
      if not initialized then
         --reinit generatorow seeden
         Rand_Int.Reset(intGenerator,seed);
         Rand_Time.Reset(todGenerator,seed);
         Ada.Numerics.Float_Random.Reset(floatGenerator,seed);
         --resetuj swiatla
         setLight;
         --losowy czas dnia
         setTime(Rand_Time.Random(todGenerator));
         --losowy stan poczatkowy swiatel
         PTrafficLight.currentState:=Integer(1.0+Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)*Float(statesTab'Length-1));
         --przelaczanie dni
         TimeOfDayClockAPI.resetCallbacks;
         TimeOfDayClockAPI.registerCallback(7*3600,setDayWrap);--zrob dzien o 7:00
         TimeOfDayClockAPI.registerCallback(21*3600,setNightWrap);--zrob noc o 21:00
         initialized:=true;
         frame:=1; --licznik ramek

         for I in spawnRatio'Range loop
            for J in spawnRatio(I)'Range loop
               --losowe wartosci produkcji samochodow z przedzialu
               spawnRatio(I)(J):=defaultSpawn(I)*(1.0+(Ada.Numerics.Float_Random.Random(Simulation.floatGenerator)-0.5)*spawnStdDev(I)*2.0);
               end loop;
         end loop;
         --reset produkcji
         for I in spawnProgress'Range loop
            spawnProgress(I):=0.0;
         end loop;
      end if;
   end;

end Simulation;
