with PRoad; use PRoad;
with PTrafficLight; use PTrafficLight;
with Simulation; use Simulation;
with Ada.Unchecked_Deallocation;
with PTimeOfDay; use PTimeOfDay;
package TaskManager is
   procedure startAll;
   procedure killAll;
   procedure pauseAll;
   procedure resumeAll;
   isRunning:Boolean:=False;
   isPaused:Boolean:=True;
private
   type TCarMoverTask_Ptr is access all TCarMoverTask;
   type lightstateChange_Task_Ptr is access all lightstateChange_Task;
   type TOD_Task_Ptr is access all TOD_Task;
   lightSynchroTask: lightstateChange_Task_Ptr;
   carMoverTask: TCarMoverTask_Ptr;
   TODClock_Task: TOD_Task_Ptr;
   procedure Free is new Ada.Unchecked_Deallocation (Object => TCarMoverTask,
                                                     Name   => TCarMoverTask_Ptr);
   procedure Free is new Ada.Unchecked_Deallocation (Object => lightstateChange_Task,
                                                     Name   => lightstateChange_Task_Ptr);
   procedure Free is new Ada.Unchecked_Deallocation (Object => TOD_Task,
                                                     Name   => TOD_Task_Ptr);
end TaskManager;
