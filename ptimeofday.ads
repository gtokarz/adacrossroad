with Ada.Containers.Hashed_Maps;
with Ada.Containers; use Ada.Containers;

package PTimeOfDay is
   --sekunda dnia
   subtype TimeOfDay is Integer range 0..86400;

   function TimeOfDay_Hashed (tod: TimeOfDay) return Hash_Type;

   type TimeOfDayEventCallback is access procedure;
   type TimeOfDayEventCallback_Wrap is record ptr:TimeOfDayEventCallback; end record;

   package PTimeOfDayEventMap is new Ada.Containers.Hashed_Maps(Key_Type        => TimeOfDay,
                                                                Element_Type    => TimeOfDayEventCallback_Wrap,
                                                                Hash            => TimeOfDay_Hashed,
                                                                Equivalent_Keys => "=",
                                                                "="             => "=");




   task type TOD_Task is
      entry Start;
      entry Stop;
      entry Pause;
      entry Resume;
   end TOD_Task;

end PTimeOfDay;
