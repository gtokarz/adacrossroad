--z racji ze nie mozna uzywac obiektow chronionych w AJIS, uzyjemy "API" ktore wrapuje metody z obiektu chronionego zegara w zwykle, dziala jak nalezy
package body PTimeOfDay.TimeOfDayClockAPI is
   protected body TimeOfDayClock is
      --zarejestruj zdarzenia o danej godzinie dnia, wykorzystywane przy zmienie trybu swiatel, ale moze byc uzyte do innych celow
      --na chwile obecna 1 zdarzenia na 1 wartosc uplywu czasu w sekundach
      procedure registerCallback(tod:TimeOfDay;callback:TimeOfDayEventCallback_Wrap) is
      begin
         eventMap.Insert(tod,callback);
      exception
         when CONSTRAINT_ERROR =>
            eventMap.Replace(tod,callback);
      end;
      procedure resetCallbacks is
      begin
         eventMap.clear;
      end resetCallbacks;
      procedure tick is
         cb:PTimeOfDayEventMap.Cursor;
      begin
         begin
            currentTOD:=currentTOD+1;
         exception
               --przekraczamy zakres - wyzeruj zegar
            when constraint_error =>
               currentTOD:=0;
         end;
         cb:=eventMap.Find(currentTOD);
         if PTimeOfDayEventMap.Has_Element(cb) then
            PTimeOfDayEventMap.Element(cb).ptr.all;
            Null;
         end if;
      end tick;
      function getTime return TimeOfDay is
      begin
         return currentTOD;
      end getTime;
      procedure setTime(val:TimeOfDay) is
      begin
         currentTOD:=val;
      end setTime;
   end TimeOfDayClock;
   --api begin
   function getTime return TimeOfDay is
   begin
      return TODClock.getTime;
   end getTime;
   procedure setTime(val:TimeOfDay) is
   begin
      TODClock.setTime(val);
   end setTime;
   procedure registerCallback(tod:TimeOfDay;callback:TimeOfDayEventCallback_Wrap) is
   begin
      TODClock.registerCallback(tod,callback);
   end registerCallback;
   procedure resetCallbacks is
   begin
      TODClock.resetCallbacks;
   end resetCallbacks;
   procedure setDay is
   begin
      dayNight:=Day;
   end setDay;
   procedure setNight is
   begin
      dayNight:=Night;
   end setNight;
end PTimeOfDay.TimeOfDayClockAPI;
