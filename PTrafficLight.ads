with PEntity;
use PEntity;
with Utils;
use Utils;
package PTrafficLight is
   type TTrafficLight;
   type TTrafficLight_Ptr is access all TTrafficLight;
   type TTrafficLight_Enum is (Red, Yellow, YellowRed, Green);

   subtype TLightCount_Range is Integer range 1..12;
   type TLightStates is array (TLightCount_Range) of TTrafficLight_Enum;
   type TLightCycle is record
      lightStates: aliased TLightStates;
      waitTime: Float :=0.0;
   end record;
   type TStatesTab is array (Integer range <>) of aliased TLightCycle;
   type TStatesWariant is array (Integer range <>) of TStatesTab (1..8);
   --tabela przelaczen, indeks stanu zgony z numerem swiatla na planszy
   statesTab: aliased TStatesWariant (1..2) :=
     (
     (--dzien
      ((Green,Red,Green,Red,Red,Green,Red,Green,Red,Green,Red,Green),35.0),
      ((Yellow,Red,Yellow,Red,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
      ((Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red),4.0),
      ((Red,YellowRed,Red,YellowRed,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
      ((Red,Green,Red,Green,Green,Red,Green,Red,Green,Red,Green,Red),35.0),
      ((Red,Yellow,Red,Yellow,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
      ((Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red),4.0),
      ((YellowRed,Red,YellowRed,Red,Red,Red,Red,Red,Red,Red,Red,Red),3.0)
     ),--noc
      (((Green,Red,Green,Red,Red,Green,Red,Green,Red,Green,Red,Green),15.0),
       ((Yellow,Red,Yellow,Red,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
       ((Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red),4.0),
       ((Red,YellowRed,Red,YellowRed,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
       ((Red,Green,Red,Green,Green,Red,Green,Red,Green,Red,Green,Red),15.0),
       ((Red,Yellow,Red,Yellow,Red,Red,Red,Red,Red,Red,Red,Red),3.0),
       ((Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red,Red),4.0),
       ((YellowRed,Red,YellowRed,Red,Red,Red,Red,Red,Red,Red,Red,Red),3.0))
     );

   type TTrafficLight is new PEntity.TEntity with
      record
         --0 czerwone, 1 zolte, 2 zielone
         actualLight : TTrafficLight_Enum;
      end record;
   procedure setLight;
   procedure setLight(tlO: out TTrafficLight;lightToSet: in TTrafficLight_Enum);
   --procedure setLight(tlO: out TTrafficLight;lightToSet: in TTrafficLight_Ptr);
   procedure Init(this: out TTrafficLight; xPoint: in Float; yPoint: in Float);

   type TLightTab is array (Integer range <>) of aliased TTrafficLight;
   --obiekt chroniony swiatel
   protected type TTrafficLightProtection is
      function getLight(index: in TLightCount_Range) return TTrafficLight_Enum;
      procedure setLight(index: in TLightCount_Range; newstate: TTrafficLight_Enum);
      function getPos(index: in TLightCount_Range) return TPoint2D;
      procedure setPos(index: in TLightCount_Range; newpos: TPoint2D);
      procedure Init(index: in TLightCount_Range;xPoint,yPoint: Float);
      function Length return Integer;
      private
      ref: aliased TLightTab (TLightCount_Range);
   end TTrafficLightProtection;

   --ada2java nie wspiera protected - proste API
   package TTrafficLightProtectionAPI is
      function getLight(index: in TLightCount_Range) return TTrafficLight_Enum;
      procedure setLight(index: in TLightCount_Range; newstate: TTrafficLight_Enum);
      function getPos(index: in TLightCount_Range) return TPoint2D;
      procedure setPos(index: in TLightCount_Range; newpos: TPoint2D);
      procedure Init(index: in TLightCount_Range;xPoint,yPoint: Float);
      function Length return Integer;
   end TTrafficLightProtectionAPI;
   lightTab: TTrafficLightProtection;
    task type lightstateChange_Task is
      entry Start;
      entry Stop;
      entry Pause;
      entry Resume;
   end lightstateChange_Task;

   currentState:Integer:=1;
   pragma Atomic(currentState);
end PTrafficLight;
