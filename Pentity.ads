--Bazowa klasa obiektu, posiada wspolrzedne x i y
with Utils;
use Utils;
package PEntity is
   type TEntity is tagged  record
      pos:aliased TPoint2D:=(0.0,0.0);
      end record;
   type TEntity_Ptr is access all TEntity;
   type Observer_Array is array(1..4) of TEntity_Ptr;
end PEntity;
