with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Calendar; use Ada.Calendar;
package Utils is
   --typ punktu
   type TPoint2D is tagged
      record
         x:Float:=0.0;
         y:Float:=0.0;
      end record;
   procedure Put(O: TPoint2D);
   --wektor
   type TVec2D is tagged
      record
         dx:Float:=0.0;
         dy:Float:=0.0;
      end record;
   procedure Put(O: TVec2D);
   --odleglosc miedzy punktami
   function distance(this: TPoint2D'Class; other: TPoint2D'Class) return Float;
   --odleglosc miedzy punktami jako wektor odleglosci
   function distanceVec(this: TPoint2D'Class; other: TPoint2D'Class) return TVec2D;
   --normalizuj wektor
   procedure normalize(this: in out TVec2D);
   --przesun punkt wg wektora
   procedure moveAlongVec(this: out TPoint2D'Class;vec: in TVec2D'Class);
   --dlugosc wektora
   function length(this: in TVec2D'Class) return Float;
   --wektor * skalar
   procedure multiply(this: out TVec2D;factor:Float);
   --minimalny czas dnia
   function MinimalTime (A, B: Time) return Time is (if A <= B then A else B);
   pragma Annotate(AJIS,Bind,MinimalTime,False);
   procedure simpleCallBack;
end Utils;
