with PCar; use PCar;
with Ada.Containers; use Ada.Containers;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;
with Simulation; use Simulation;
with Ada.Exceptions; use Ada.Exceptions;
with PTimeOfDay.TimeOfDayClockAPI; use PTimeOfDay.TimeOfDayClockAPI;
package body PRoad is
   procedure spawnCar(spawnPointIndex: in Integer) is
      newCar:TCar_Ptr:=null;
      laneLastCar:TCar_Ptr:=null;
   begin
      newCar:=new TCar;
      newCar.Init;

      newCar.laneIndex:=spawnPointIndex;
      newCar.pitch:=spawnPoints(spawnPointIndex).trajectory;
      newCar.pos:=spawnPoints(spawnPointIndex).pos;
      if carList(spawnPointIndex).Length > 0 then
         laneLastCar:=carList(spawnPointIndex).Last_Element;
         --some car is following us
         laneLastCar.followingCar:=newCar;
      end if;

      --new car is following last one
      newCar.leadingCar:=laneLastCar;
      carList(spawnPointIndex).Append(New_Item => newCar,
                                      Count    => 1);

   end spawnCar;
   task body TCarMoverTask is
      Next_Time : Time;
      J: Integer:=1;
      C: PCarList.Cursor;
      deleteC: PCarList.Cursor;
      carPtr:TCar_Ptr;
      isPaused:Boolean:=False;
      --posprzataj plansze
      procedure stopMe is
      begin
         lockCars;
         for I in Integer range 1..carList'Length loop
            if carList(I).Length > 0 then
               C:=carList(I).First;
               while PCarList.Has_Element (C) loop
                  carPtr:=PCarList.Element(C);
                  Free(carPtr);
                  PCarList.Next (C);
               end loop;
               carList(I).Clear;
            end if;
         end loop;
         unlockCars;
      end;
   begin
      main_loop:
      loop
         select
            accept Start;
         or
            accept Resume;
         or
            accept Stop;
            stopMe;
            exit main_loop;
         end select;
         loop
            Next_Time := Clock + Duration(1.0/Simulation.stepsPerSecond);
            --porusz samochody
            -- dla kazdego pasa
            for I in Integer range 1..carList'Length loop
               if carList(I).Length>0 then
                  C:=carList(I).First;
                  --dla kazdego samochodu
                  while PCarList.Has_Element (C) loop
                     carPtr:=PCarList.Element(C);
                     --jesli trzeba usunac auto (wyjechalo za plansze to tak zrob)
                     if carPtr.toDelete then
                        lockCars;--semafor bo java nas zje
                        deleteC:=C; --kopia cursora
                        PCarList.Next (C); --pozycja za kursorem
                        carList(I).Delete(Position  => deleteC,
                                          Count     => 1);
                        Free(carPtr);
                        unlockCars;--semafor w górę - pozwól rysować samochody
                     else
                        --a jak nie usuwamy to je poruszamy
                        carPtr.move;
                        PCarList.Next (C);
                     end if;
                  end loop;
               end if;

            end loop;
            --spawnuj nowe
            --dla kazdej pozycji startowej oblicz postep "produkcji auta"
            for I in spawnProgress'Range loop
               --dodaj wartosc produkcji do bufora
               spawnProgress(I):=spawnProgress(I)+scaleTimeDomain(spawnRatio(TTrafficLight_DayNight'Pos(dayNight)+1)(I));
               --mamy gotowe auto
               if spawnProgress(I)>=1.0 then
                  --dodajemy do listy zablokuj ja semaforem
                  lockCars;
                  C:=carList(I).Last;
                  --lista jest pusta dodaj
                  if not PCarList.Has_Element(C) then
                     spawnCar(I);
                     spawnProgress(I):=spawnProgress(I)-1.0;
                     --jesli lista nie jest pusta i jest miejsce na pozycji startowej to dodaj auto
                  elsif PCarList.Has_Element(C) and PCarList.Element(C).pos/=spawnPoints(I).pos then
                     spawnCar(I);
                     spawnProgress(I):=spawnProgress(I)-1.0;
                  end if;
                  unlockCars;
               end if;
            end loop;
            delay until Next_Time;
            Simulation.frame:=Simulation.frame+1;
            Select
               accept Stop;
               stopMe;
               exit main_loop;
            or
               accept Pause;
               exit;
            else
               Null;
            end select;
         end loop;
      end loop main_loop;
      --todo fixme
   exception
      when Error:others =>
         Put_Line("It shouldn't happen: "&Exception_Information(Error)&":"&Exception_Message(Error));
   end TCarMoverTask;
   --typ semafora, prosty semafor wykonany
   protected body Signal_Object is

      entry Wait when Open is
      begin
         Open := False;
      end Wait;

      procedure Signal is
      begin
         Open := True;
      end Signal;

      function Is_Open return Boolean is
      begin
         return Open;
      end Is_Open;

   end Signal_Object;
   procedure lockCars is
   begin
      carLock.Wait;
   end lockCars;
   procedure unlockCars is
   begin
      carLock.Signal;
   end unlockCars;
end PRoad;
