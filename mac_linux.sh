#!/bin/bash
echo "[1] CLEAN UP..."
rm -rf glue_code
rm -rf java_code/src
echo -e "done\n"
echo -e "[2] ENV SETUP...\n"
#export GNAT_PATH=/usr/local/gnat2013
#gnat install dir
export GNAT_PATH=/D/GNAT/2014
#AJIS JAR LOCATION
export AJIS_JAR=$GNAT_PATH/lib/ajis.jar
#native lib java lookup
export LD_LIBRARY_PATH=lib
#where are gprs located
export GPR_PROJECT_PATH=$GNAT_PATH/lib/gnat
#same as above - legacy option
export ADA_PROJECT_PATH=$GNAT_PATH/lib/gnat
#java class path + resources path
export CLASSPATH=$AJIS_JAR:java_code/src:java_gui/src:java_gui/resources

env | grep -e '^GNAT_PATH\|^AJIS_JAR\|^LD_LIBRARY_PATH\|^GPR_PROJECT_PATH\|^ADA_PROJECT_PATH\|^CLASSPATH'
echo -e "done\n"
echo -e "[3] Generating java binding..."
ada2java *.ads -L hello_proj -c java_code/src -o glue_code --library-kind=encapsulated > generate.log
echo -e "done\n"
echo -e "[3] Patching sources java binding..."
patch < patches/libraryLoaderPatch.patch -p 1
patch < patches/asyncTaskManager.patch -p 1
echo -e "done\n"
echo -e "[4] Compiling java..."
find . -type f -name "*.java" | xargs javac -cp $AJIS_JAR
echo -e "done\n"
echo -e "[5] PrePatching ajis.gpr & jni.gpr..."
patch -d $GPR_PROJECT_PATH <patches/ajis.patch
patch -d $GPR_PROJECT_PATH <patches/jni.patch
echo -e "done...Building lib..."
gprbuild -j10 -p -P glue_code/hello_proj.gpr -gnat2012
echo -e "done...copying to lib"
cp glue_code/lib/* lib
echo -e "done...reverting ajis & jni patch..."
patch -d $GPR_PROJECT_PATH -R <patches/ajis.patch
patch -d $GPR_PROJECT_PATH -R <patches/jni.patch
echo -e "done\n"
echo -e "[6] Preparing to launch"
java -cp $CLASSPATH sim.Gui
echo -e "done\n"
