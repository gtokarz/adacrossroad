/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim;

import PTimeOfDay.TimeOfDayClockAPI.TTrafficLight_DayNight;
import PTimeOfDay.TimeOfDayClockAPI.TimeOfDayClockAPI_Package;
import Simulation.Simulation_Package;
import TaskManager.TaskManager_Package;

import javax.swing.*;

public class Gui {
    public static void main(String[] args) {
        try {
            System.out.println("GUI start");
            //defaultowe parametry
            Simulation_Package.speed(4.0);
            Simulation_Package.stepsPerSecond(20);
            Simulation_Package.Init();
            TimeOfDayClockAPI_Package.dayNight(TTrafficLight_DayNight.Night);
            TaskManager_Package.startAll();


            MainFrame st = new MainFrame();
            //główna pętla - odświeża gui
            while (true) {
                st.repaint();
                try {
                    Thread.sleep(50L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "Wyjątek:"+e.getMessage());
        }
    }
}
