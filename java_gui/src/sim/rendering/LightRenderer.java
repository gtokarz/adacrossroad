/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.rendering;

import PTrafficLight.TTrafficLightProtectionAPI.TTrafficLightProtectionAPI_Package;
import PTrafficLight.TTrafficLight_Enum;
import Utils.TPoint2D;
import sim.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Created by GT on 2015-01-09.
 */
public class LightRenderer extends JPanel {
    protected int iconCount = 4;
    protected Image img[] = new Image[iconCount];
    public LightRenderer(){
        for (int i = 0; i < iconCount; i++) {
            img[i] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/lights/" + (i + 1) + ".png"));
        }
    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        for(int i=1;i<= TTrafficLightProtectionAPI_Package.Length();i++){
            TPoint2D pos = TTrafficLightProtectionAPI_Package.getPos(i);
            drawLight(g, pos.x(), pos.y(), TTrafficLightProtectionAPI_Package.getLight(i));
            g.drawString(Integer.toString(i), Utils.fieldPos2Px(pos.x())+10, Utils.fieldPos2Px(pos.y()));
        }

    }
    private void drawLight(Graphics g,double x,double y, TTrafficLight_Enum state){
        int posX= Utils.fieldPos2Px(x);
        int posY= Utils.fieldPos2Px(y);
        int width= Utils.fieldPos2Px(1.032);//86/100*1.2
        int height= Utils.fieldPos2Px(2.5799999999999996);//215/100*1.2
        BufferedImage resized = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gf = resized.createGraphics();

        gf.setComposite(AlphaComposite.Clear);
        gf.fillRect(0, 0, (int) width, (int) height);
        gf.setComposite(AlphaComposite.Src);
        gf.drawImage(img[state.ordinal()], 0, 0, (int) width, (int) height, null);
        gf.dispose();

        AffineTransform at = new AffineTransform();
        at.translate(posX - width / 2.0, posY - height / 2.0);

        Graphics2D gg = (Graphics2D) g.create();
        gg.drawImage(resized, at, null);
    }
}
