/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.rendering;

import PCar.PCar_Package;
import PTimeOfDay.TimeOfDayClockAPI.TimeOfDayClockAPI_Package;
import Simulation.Simulation_Package;
import sim.MainFrame;
import sim.Utils;
import sim.gui.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel konfiguracyjny z walidacją danych wejściowych
 *
 * Created by Sebbatko on 2015-01-09.
 *
 */
public class ConfigurationPanel extends JPanel {
    private int configPairs=0;

    private JTextField seedField;
    private JTextField stepsPerSecondField;
    private JTextField speedField;
    private JTextField accelerationSTDdevField;
    private JTextField decelerationSTDdevField;
    private JTextField velocityCapSTDdevField;
    private JTextField minDistanceSTDdevField;
    private JTextField defaultAccelerationField;
    private JTextField defaultDecelerationField;
    private JTextField defaultVelocityCapField;
    private JTextField defaultHeadwayField;
    private JTextField deltaExponentField;
    private JTextField defaultMinDistanceField;
    private JTextField defaultLengthField;
    private JTextField lendthSTDdevField;
    private JTextField timeOfDay;

    private JScrollPane scroller;
    private JPanel panel=new JPanel();


    private Integer seed = Simulation_Package.seed();
    private Double stepsPerSecond = Simulation_Package.stepsPerSecond();
    private Double speed = Simulation_Package.speed();
    private Double accelerationSTDdev = PCar_Package.accelerationSTDdev()*100;
    private Double decelerationSTDdev = PCar_Package.decelerationSTDdev()*100;
    private Double velocityCapSTDdev = PCar_Package.velocityCapSTDdev()*100;
    private Double minDistanceSTDdev = PCar_Package.minDistanceSTDdev()*100;
    private Double defaultAcceleration = PCar_Package.defaultAcceleration();
    private Double defaultDeceleration = PCar_Package.defaultDeceleration();
    private Double defaultVelocityCap = PCar_Package.defaultVelocityCap();
    private Double defaultHeadway = PCar_Package.defaultHeadway();
    private Double deltaExponent = PCar_Package.deltaExponent();
    private Double defaultMinDistance = PCar_Package.defaultMinDistance();
    private Double defaultLength = PCar_Package.defaultLength();
    private Double lendthSTDdev = PCar_Package.lendthSTDdev()*100;


    public ConfigurationPanel(){

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        panel.setLayout(new SpringLayout());
        //scroller.add(panel);

        seedField = configEntry("Seed: ", Utils.padRight(" ", 300),new JTextField(30));
        stepsPerSecondField = configEntry("Steps per Second: ",Utils.padRight(" ", 300),new JTextField(30));
        speedField = configEntry("Speed: ",Utils.padLeft(" ",300),new JTextField(30));
        accelerationSTDdevField = configEntry("accelerationSTDdev: ",Utils.padRight("%", 300),new JTextField(30));
        decelerationSTDdevField = configEntry("decelerationSTDdev: ",Utils.padRight("%", 300),new JTextField(30));
        velocityCapSTDdevField = configEntry("velocityCapSTDdev: ",Utils.padRight("%", 300),new JTextField(30));
        minDistanceSTDdevField = configEntry("minDistanceSTDdev: ",Utils.padRight("%", 300),new JTextField(30));
        defaultAccelerationField = configEntry("defaultAcceleration: ",Utils.padRight("m/s^2", 300),new JTextField(30));
        defaultDecelerationField = configEntry("defaultDeceleration: ",Utils.padRight("m/s^2", 300),new JTextField(30));
        defaultVelocityCapField = configEntry("defaultVelocityCap: ",Utils.padRight("m/s", 300),new JTextField(30));
        defaultHeadwayField = configEntry("defaultHeadway: ",Utils.padRight("s", 300),new JTextField(30));
        deltaExponentField = configEntry("deltaExponent: ",Utils.padRight(" ", 300),new JTextField(30));
        defaultMinDistanceField = configEntry("defaultMinDistance: ",Utils.padRight("m", 300),new JTextField(30));
        defaultLengthField = configEntry("defaultLength: ",Utils.padRight("m", 300),new JTextField(30));
        lendthSTDdevField = configEntry("lendthSTDdev: ",Utils.padRight("%", 300),new JTextField(30));
        timeOfDay = configEntry("Time of day: ",Utils.padRight("Format HH:MM:SS", 300),new JTextField(30));
        refresh();

        SpringUtilities.makeCompactGrid(panel,
                configPairs, 3, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        scroller=new JScrollPane(panel);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setMaximumSize(new Dimension(MainFrame.width, MainFrame.height / 2));

        add(scroller);
        JButton submitButton = new JButton("Save");
        submitButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e)
            {
                try {
                    seed = Integer.parseInt(seedField.getText());
                    Simulation_Package.seed(seed);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru seed");
                    seedField.setText(""+seed);
                }

                try {
                    stepsPerSecond = Double.parseDouble(stepsPerSecondField.getText());
                    Simulation_Package.stepsPerSecond(stepsPerSecond);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru stepsPerSecond");
                    stepsPerSecondField.setText(""+stepsPerSecond);
                }


                try{
                    speed = Double.parseDouble(speedField.getText());
                    Simulation_Package.speed(speed);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru speed");
                    speedField.setText(""+speed);
                }

                try{
                    accelerationSTDdev = Double.parseDouble(accelerationSTDdevField.getText())/100;
                    PCar_Package.accelerationSTDdev();
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru accelerationSTDdev");
                    accelerationSTDdevField.setText(""+accelerationSTDdev);
                }

                try{
                    decelerationSTDdev = Double.parseDouble(decelerationSTDdevField.getText())/100;
                    PCar_Package.decelerationSTDdev(decelerationSTDdev);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru decelerationSTDdev");
                    decelerationSTDdevField.setText(""+decelerationSTDdev);
                }

                try{
                    velocityCapSTDdev = Double.parseDouble(velocityCapSTDdevField.getText())/100;
                    PCar_Package.velocityCapSTDdev(velocityCapSTDdev);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru velocityCapSTDdev");
                    velocityCapSTDdevField.setText(""+velocityCapSTDdev);
                }

                try{
                    minDistanceSTDdev = Double.parseDouble(minDistanceSTDdevField.getText())/100;
                    PCar_Package.minDistanceSTDdev(minDistanceSTDdev);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru minDistanceSTDdev");
                    minDistanceSTDdevField.setText(""+minDistanceSTDdev);
                }

                try{
                    defaultAcceleration = Double.parseDouble(defaultAccelerationField.getText());
                    PCar_Package.defaultAcceleration(defaultAcceleration);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultAcceleration");
                    defaultAccelerationField.setText(""+defaultAcceleration);
                }

                try{
                    defaultDeceleration = Double.parseDouble(defaultDecelerationField.getText());
                    PCar_Package.defaultDeceleration(defaultDeceleration);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultDeceleration");
                    defaultDecelerationField.setText(""+defaultDeceleration);
                }

                try{
                    defaultVelocityCap = Double.parseDouble(defaultVelocityCapField.getText());
                    PCar_Package.defaultVelocityCap(defaultVelocityCap);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultVelocityCap");
                    defaultVelocityCapField.setText(""+defaultVelocityCap);
                }

                try{
                    defaultHeadway = Double.parseDouble(defaultHeadwayField.getText());
                    PCar_Package.defaultHeadway(defaultHeadway);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultHeadway");
                    defaultHeadwayField.setText(""+defaultHeadway);
                }

                try{
                    deltaExponent = Double.parseDouble(deltaExponentField.getText());
                    PCar_Package.deltaExponent(deltaExponent);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru deltaExponent");
                    deltaExponentField.setText(""+deltaExponent);
                }

                try{
                    defaultMinDistance = Double.parseDouble(defaultMinDistanceField.getText());
                    PCar_Package.defaultMinDistance(defaultMinDistance);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultMinDistance");
                    defaultMinDistanceField.setText(""+defaultMinDistance);
                }

                try{
                    defaultLength = Double.parseDouble(defaultLengthField.getText());
                    PCar_Package.defaultLength(defaultLength);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru defaultLength");
                    defaultLengthField.setText(""+defaultLength);
                }

                try{
                    lendthSTDdev = Double.parseDouble(lendthSTDdevField.getText());
                    PCar_Package.lendthSTDdev(lendthSTDdev);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu parametru lendthSTDdev");
                    lendthSTDdevField.setText(""+lendthSTDdev);
                }
                try {
                    String[] chunks = timeOfDay.getText().split(":");
                    int[] hhmmss = {Integer.parseInt(chunks[0]), Integer.parseInt(chunks[1]), Integer.parseInt(chunks[2])};
                    //range check
                    if (hhmmss[0] < 0 || hhmmss[0] > 23 || hhmmss[1] < 0 || hhmmss[1] > 59 || hhmmss[0] < 2 || hhmmss[2] > 59) {
                        throw new IllegalArgumentException("Wrong hh:mm:ss format");
                    }
                    TimeOfDayClockAPI_Package.setTime(hhmmss[0] * 3600 + hhmmss[1] * 60 + hhmmss[2]);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Wpisano złą wartość w polu Time of day"+ex.getMessage());
                    timeOfDay.setText(Utils.formatTime(TimeOfDayClockAPI_Package.getTime()));
                }
            }
        });

        add(submitButton);

    }
    public void refresh(){

        seed = Simulation_Package.seed();
        stepsPerSecond = Simulation_Package.stepsPerSecond();
        speed = Simulation_Package.speed();
        accelerationSTDdev = PCar_Package.accelerationSTDdev()*100;
        decelerationSTDdev = PCar_Package.decelerationSTDdev()*100;
        velocityCapSTDdev = PCar_Package.velocityCapSTDdev()*100;
        minDistanceSTDdev = PCar_Package.minDistanceSTDdev()*100;
        defaultAcceleration = PCar_Package.defaultAcceleration();
        defaultDeceleration = PCar_Package.defaultDeceleration();
        defaultVelocityCap = PCar_Package.defaultVelocityCap();
        defaultHeadway = PCar_Package.defaultHeadway();
        deltaExponent = PCar_Package.deltaExponent();
        defaultMinDistance = PCar_Package.defaultMinDistance();
        defaultLength = PCar_Package.defaultLength();
        lendthSTDdev = PCar_Package.lendthSTDdev()*100;

        seedField.setText(""+seed);
        stepsPerSecondField.setText(""+stepsPerSecond);
        speedField.setText(""+speed);
        accelerationSTDdevField.setText(""+accelerationSTDdev);
        decelerationSTDdevField.setText(""+decelerationSTDdev);
        velocityCapSTDdevField.setText(""+velocityCapSTDdev);
        minDistanceSTDdevField.setText(""+minDistanceSTDdev);
        defaultAccelerationField.setText(""+defaultAcceleration);
        defaultDecelerationField.setText(""+defaultDeceleration);
        defaultVelocityCapField.setText(""+defaultVelocityCap);
        defaultHeadwayField.setText(""+defaultHeadway);
        deltaExponentField.setText(""+deltaExponent);
        defaultMinDistanceField.setText(""+defaultMinDistance);
        defaultLengthField.setText(""+defaultLength);
        lendthSTDdevField.setText(""+lendthSTDdev);
        timeOfDay.setText(Utils.formatTime(TimeOfDayClockAPI_Package.getTime()));
    }
    private JTextField configEntry(String label,JTextField widget){
        return (JTextField)configEntry(label,null,(JComponent)widget);
    }
    private JTextField configEntry(String label,String valueUnit,JTextField widget){
        return (JTextField)configEntry(label,valueUnit,(JComponent)widget);
    }
    private JComponent configEntry(String label,JComponent widget){
        return configEntry(label,null,widget);
    }
    private JComponent configEntry(String label,String valueUnit,JComponent widget){
        JLabel l = new JLabel(label, JLabel.TRAILING);
        panel.add(l);
        l.setLabelFor(widget);
        panel.add(widget);
        Dimension d=widget.getMaximumSize();
        d.setSize(d.getWidth(),20);
        widget.setMaximumSize(d);
        l = new JLabel(valueUnit, JLabel.TRAILING);
        panel.add(l);
        l.setLabelFor(widget);
        configPairs++;
        return widget;
    }
}
