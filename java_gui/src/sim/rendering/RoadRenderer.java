/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.rendering;

import sim.Utils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GT on 2015-01-09.
 */
public class RoadRenderer extends JPanel {
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.gray);
        int posX= Utils.fieldPos2Px(37.0);
        int posY= Utils.fieldPos2Px(0.0);
        int width= Utils.fieldPos2Px(6.0);
        int height= Utils.fieldPos2Px(80.0);
        g.fillRect(posX,posY,width,height);
        g.fillRect(posY,posX,height,width);

        //pasy ruchu
        Graphics2D g2 = (Graphics2D) g;
        float dash[] = { 10.0f };
        Stroke oldStroke=g2.getStroke();
        g2.setStroke(new BasicStroke(Utils.fieldPos2Px(0.2), BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL, 10.0f, dash, 0.0f));

        posX= Utils.fieldPos2Px(40.0);
        posY= Utils.fieldPos2Px(37.0);
        g2.setPaint(Color.white);
        g2.drawLine(posX, Utils.fieldPos2Px(0),posX,posY);

        posX= Utils.fieldPos2Px(40.0);
        posY= Utils.fieldPos2Px(80.0);
        g2.drawLine(posX, Utils.fieldPos2Px(43.0),posX,posY);

        posX= Utils.fieldPos2Px(0.0);
        posY= Utils.fieldPos2Px(40.0);
        g2.drawLine(posX,posY, Utils.fieldPos2Px(37.0),posY);

        posX= Utils.fieldPos2Px(43.0);
        posY= Utils.fieldPos2Px(40.0);
        g2.drawLine(posX,posY, Utils.fieldPos2Px(80.0),posY);

        //przejscia dla pieszych :)
        g2.setStroke(new BasicStroke(Utils.fieldPos2Px(4), BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
        posX= Utils.fieldPos2Px(37.0);
        posY= Utils.fieldPos2Px(35.0);
        g2.drawLine(posX,posY, Utils.fieldPos2Px(43),posY);

        posX= Utils.fieldPos2Px(37.0);
        posY= Utils.fieldPos2Px(45.0);
        g2.drawLine(posX,posY, Utils.fieldPos2Px(43),posY);

        posX= Utils.fieldPos2Px(35.0);
        posY= Utils.fieldPos2Px(37.0);
        g2.drawLine(posX,posY,posX, Utils.fieldPos2Px(43));

        posX= Utils.fieldPos2Px(45.0);
        posY= Utils.fieldPos2Px(37.0);
        g2.drawLine(posX,posY,posX, Utils.fieldPos2Px(43));
        g2.setStroke(oldStroke);
    }
}
