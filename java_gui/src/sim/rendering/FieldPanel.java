/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.rendering;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GT on 2015-01-11.
 */
public class FieldPanel extends JLayeredPane {
    RoadRenderer road=new RoadRenderer();
    LightRenderer lights=new LightRenderer();
    CarRenderer cars=new CarRenderer();
    public FieldPanel(){
        super();
    }
    public void setupComponents(){
        int width= (int) getSize().getWidth();
        int height= (int) getSize().getHeight();
        road.setBounds(0, 0, width, height);
        road.setPreferredSize(getSize());
        add(road);
        road.setOpaque(true);

        lights.setBounds(0, 0, width, height);
        lights.setPreferredSize(getSize());
        add(lights);
        lights.setOpaque(true);

        cars.setBounds(0, 0, width, height);
        cars.setPreferredSize(getSize());
        add(cars);
        cars.setOpaque(true);

        lights.setBackground(new Color(0,0,0,0));
        cars.setBackground(new Color(0,0,0,0));

        setLayer(road,0);
        setLayer(lights,1);
        setLayer(cars,2);

    }
}
