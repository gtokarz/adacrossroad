/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.rendering;

import PCar.TCar;
import PRoad.PCarList.PCarList_Package;
import PRoad.PRoad_Package;
import sim.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Created by GT on 2015-01-09.
 */
public class CarRenderer extends JPanel {
    protected static int iconCount = 7;
    protected static Image img[] = new Image[iconCount];
    static {
        for (int i = 0; i < iconCount; i++) {
            img[i] = Toolkit.getDefaultToolkit().getImage(CarRenderer.class.getResource("/car/" + (i + 1) + ".png"));
        }
    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        PRoad.PCarList.Cursor C;
        TCar car;
        //zablokuj semafor (obiekt chroniony), jest on uzywany do trzymania listy w stanie jednolitym miedzy wątkami ady i javy, zapobiega m.in Access Violation przy usuwaniu z listy
        PRoad_Package.lockCars();
        //dla kazdego pasa
        for(int i=1;i<= PRoad_Package.carList().Length();i++){
            C=PRoad_Package.carList().Get_Element_At(i).First();
            //dla kazdego auta
            while(PCarList_Package.Has_Element(C)){
                car=PCarList_Package.Element(C);
                //namaluj je
                drawCar(g,car);
                C=PCarList_Package.Next(C);
            }
        }
        PRoad_Package.unlockCars();
    }
    private void drawCar(Graphics g,TCar car){
        double x=car.pos().x();
        double y=car.pos().y();
        int posX= Utils.fieldPos2Px(x);
        int posY= Utils.fieldPos2Px(y);
        double pitch=Math.atan2(car.pitch().dx(), car.pitch().dy());
        int width= Utils.fieldPos2Px(2.0);
        int height= Utils.fieldPos2Px(car.length());
        BufferedImage resized = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gf = resized.createGraphics();
        //gf.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        //gf.setPaint(Color.red);
        gf.setComposite(AlphaComposite.Clear);
        gf.fillRect(0, 0, (int) width, (int) height);
        gf.setComposite(AlphaComposite.Src);
        gf.drawImage(img[car.icon()-1], 0, 0, (int) width, (int) height, null);
        gf.dispose();
        if (car.pitch().dy() !=0.0) {
            pitch-=Math.toRadians(180);
        }
        AffineTransform at = new AffineTransform();
        at.translate(posX - width / 2.0, posY - height / 2.0);

        at.rotate(pitch, width / 2.0, height / 2.0);
        Graphics2D gg = (Graphics2D) g.create();
        gg.drawImage(resized, at, null);
    }
}
