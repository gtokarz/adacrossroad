/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim;

import TaskManager.TaskManager_Package;
import sim.gui.DayNightButton;
import sim.gui.SiminfoPanel;
import sim.rendering.ConfigurationPanel;
import sim.rendering.FieldPanel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame{
    public static int width=500;
    public static int height=500;

    FieldPanel panel=new FieldPanel();;
    JTabbedPane tabs=new JTabbedPane();
    JPanel infoPanel = new JPanel();
    JPanel siminfo=new SiminfoPanel();
    JButton dnBtn=new DayNightButton();
    public MainFrame(){
        width= (int) GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth();
        height= (int) GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getHeight();

        setContentPane(tabs);
        panel.setSize(new Dimension(height, height));
        panel.setupComponents();
        infoPanel.setLayout(new BorderLayout());
        infoPanel.add(panel, BorderLayout.CENTER);
        infoPanel.add(siminfo,BorderLayout.PAGE_END);
        infoPanel.add(dnBtn,BorderLayout.LINE_END);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gui/appicon.png")));
        setTitle("Symulacja skrzyżowania - Ada+Java - PWIR 2014/2015 G.Tokarz S.Batko");

        tabs.addTab("Przebieg symulacji",infoPanel);
        final ConfigurationPanel config;
        tabs.addTab("Konfiguracja",config=new ConfigurationPanel());
        tabs.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                switch (tabs.getSelectedIndex())
                {
                    case 1:
                        TaskManager_Package.pauseAll();
                        config.refresh();
                        break;
                    case 0:
                        TaskManager_Package.resumeAll();
                        break;
                }

            }
        });
        setupFieldRendering();
        tabs.setPreferredSize(new Dimension(width, height));

        setLocationRelativeTo(null);
        pack();
        //handler zamykania okna
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                //zatrzymaj taski Ady
                TaskManager_Package.killAll();
                System.exit(0);
            }
        });
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(true);
    }
    private void setupFieldRendering(){

    }
    public void repaint(){
        super.repaint();
        siminfo.repaint();
        dnBtn.repaint();
    }

}