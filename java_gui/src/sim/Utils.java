/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim;
/**
 * Created by GT on 2015-01-11.
 */

public class Utils {
    public static int fieldPos2Px(double value)
    {
        return (int)(value/80.0 * Math.min(MainFrame.width,MainFrame.height));
    }
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }
    public static String formatTime(int totalSecs){
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
