/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.gui;

import Simulation.Simulation_Package;

import javax.swing.*;

/**
 * Created by GT on 2015-01-12.
 */
public class SiminfoPanel extends JPanel {
    JLabel simstep;
    JLabel speedinfo;
    JLabel stepinfo;
    public SiminfoPanel(){
        super();
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        simstep=new JLabel("---");
        speedinfo=new JLabel("---");
        stepinfo=new JLabel("---");
        add(simstep);
        add(speedinfo);
        add(stepinfo);
        add(new PauseButton());
        add(new StartButton());
    }
    public void repaint(){
        super.repaint();
        if(speedinfo!=null)
            speedinfo.setText("Speed: x"+Double.toString(Simulation_Package.speed())+"     ");
        if(stepinfo!=null)
            stepinfo.setText("Step length: "+Double.toString(1000/Simulation_Package.stepsPerSecond())+"ms     ");
    }
}
