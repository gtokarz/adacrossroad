/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.gui;

import PTimeOfDay.TimeOfDayClockAPI.TTrafficLight_DayNight;
import PTimeOfDay.TimeOfDayClockAPI.TimeOfDayClockAPI_Package;
import sim.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by GT on 2015-01-14.
 */
public class DayNightButton extends JButton {
    protected static int iconCount = 2;
    protected static ImageIcon img[] = new ImageIcon[iconCount];

    static {
        for (int i = 0; i < iconCount; i++) {
            img[i] = new ImageIcon(Toolkit.getDefaultToolkit().getImage(DayNightButton.class.getResource("/gui/daynight/" + (i + 1) + ".png")));
        }
    }
    public DayNightButton(){
        super(img[TimeOfDayClockAPI_Package.dayNight().ordinal()]);
        setOpaque(true) ; //changed false from true.
        setFocusPainted(true) ; //changed false from true.
        setVerticalTextPosition   ( SwingConstants.BOTTOM ) ;
        setVerticalAlignment      ( SwingConstants.CENTER ) ;
        setHorizontalTextPosition(SwingConstants.CENTER) ;
        setFont(new Font("Arial", Font.PLAIN, 40));

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (TimeOfDayClockAPI_Package.dayNight()== TTrafficLight_DayNight.Day)
                    TimeOfDayClockAPI_Package.setNight();
                else
                    TimeOfDayClockAPI_Package.setDay();;
            }
        });
    }
    public void repaint(){
        setText(Utils.formatTime(TimeOfDayClockAPI_Package.getTime()));
        setIcon(img[TimeOfDayClockAPI_Package.dayNight().ordinal()]);
        super.repaint();
    }
}
