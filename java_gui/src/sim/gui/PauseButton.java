/**
 * Projekt skrzyżowanie ze światłami
 * @author: Grzegorz Tokarz
 * @author: Sebastian Batko
 * 15.01.2015
 */
package sim.gui;

import TaskManager.TaskManager_Package;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by GT on 2015-01-12.
 */
public class PauseButton extends JButton {

    public PauseButton() {
        super();
        setText((TaskManager_Package.isPaused())?"Resume":"Pause");
        final JButton b=this;
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                new Thread(
                        new Runnable() {
                            public void run() {
                                b.setEnabled(false);
                                if(TaskManager_Package.isPaused())
                                    TaskManager_Package.resumeAll();
                                else
                                    TaskManager_Package.pauseAll();
                                b.setEnabled(true);
                                b.setText((TaskManager_Package.isPaused())?"Resume":"Pause");
                            }
                        }
                ).start();
            }
        });
    }
}
