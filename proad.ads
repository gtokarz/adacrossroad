with PCar; use PCar;
with Utils; use Utils;
with Ada.Containers.Doubly_Linked_Lists;

package PRoad is
   --punkt startowy
   type TSpawnPoint is record
      pos: aliased TPoint2D:=(0.0,0.0);
      trajectory: aliased TVec2D:=(0.0,0.0);
   end record;
   --pozycje startowe na pasach
   type TSpawnPointArray is array (Integer range <>) of aliased TSpawnPoint;

   spawnPoints:aliased TSpawnPointArray (1..4):=(
                                                 ((38.5,0.0),(0.0,1.0)),
                                                 ((80.0,38.5),(-1.0,0.0)),
                                                 ((41.5,80.0),(0.0,-1.0)),
                                                 ((0.0,41.5),(1.0,0.0))

                                                );
   type TSpawnValue is array (Integer range<>) of aliased Float;

   type TSpawnTable is array (Integer range <>) of TSpawnValue (1..4);
   --wspolczynniki produkcji
   spawnRatio:aliased TSpawnTable(1..2):=((0.0,0.0,0.0,0.0),(0.0,0.0,0.0,0.0));
   --postep produkcji
   spawnProgress:aliased TSpawnValue(1..4):=(0.0,0.0,0.0,0.0);
   package PCarList is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => TCar_Ptr,
      "="          => "=");
   pragma Annotate (AJIS, Rename, PCarList.Next, "Next");
   pragma Annotate (AJIS, Rename, PCarList.Previous, "Previous");

   type TRoadDirections is array (Integer range <>) of aliased PCarList.List;
   carList: aliased TRoadDirections(1..4);

   procedure spawnCar(spawnPointIndex: Integer);

   task type TCarMoverTask is
      entry Start;
      entry Stop;
      entry Pause;
      entry Resume;
   end TCarMoverTask;
   defaultSpawn:TSpawnValue(1..2):=(0.4,0.2);

   spawnStdDev:TSpawnValue(1..2):=(0.5,0.5);
   protected type Signal_Object is
      entry Wait;
      procedure Signal;
      function Is_Open return Boolean;
   private
      Open : Boolean := True;
   end Signal_Object;

   carLock:Signal_Object;

   procedure lockCars;
   procedure unlockCars;
end PRoad;
